package io.dante.virgil

import io.dante.virgil.ui.view.LoginView
import javafx.application.Application
import tornadofx.App
import tornadofx.importStylesheet

class Virgil : App(LoginView::class) {
    init {
        importStylesheet("/styles/Styles.css")
    }
}

fun main(args: Array<String>) = Application.launch(Virgil::class.java, *args)