package io.dante.virgil.jira

class Issue(val key: String, val summary: String, val dateCreated: String, val dateUpdated: String) {
}