package io.dante.virgil.ui.model

import io.dante.virgil.jira.Issue
import tornadofx.ItemViewModel

class IssueModel : ItemViewModel<Issue>() {
    val key = bind(Issue::key)
    val summary = bind(Issue::summary)
    val dateCreated = bind(Issue::dateCreated)
    val dateUpdated = bind(Issue::dateUpdated)
}