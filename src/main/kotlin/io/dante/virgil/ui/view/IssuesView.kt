package io.dante.virgil.ui.view

import io.dante.virgil.ui.controller.IssuesController
import tornadofx.View
import tornadofx.action
import tornadofx.label
import tornadofx.listview
import tornadofx.togglebutton
import tornadofx.vbox

class IssuesView : View() {
    val controller: IssuesController by inject()

    override val root = vbox {
        styleClass.add("vbox")

        label("Virgil") {
            id = "app-header"
        }

        listview(controller.issues) {
            cellFragment(IssueItemFragment::class)
        }

        togglebutton ("Start") {
            action {
                when {
                    this.isSelected -> {
                        this.text = "Stop"

                        controller.startWatching()
                    }
                    else -> {
                        this.text = "start"

                        controller.stopWatching()
                    }
                }
            }
        }
    }
}