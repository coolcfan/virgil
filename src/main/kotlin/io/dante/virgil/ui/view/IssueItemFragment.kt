package io.dante.virgil.ui.view

import io.dante.virgil.jira.Issue
import io.dante.virgil.ui.model.IssueModel
import tornadofx.ListCellFragment
import tornadofx.bindTo
import tornadofx.field
import tornadofx.fieldset
import tornadofx.form
import tornadofx.label

class IssueItemFragment : ListCellFragment<Issue>() {
    val issue = IssueModel().bindTo(this)

    override val root = form {
        fieldset {
            field(text = "Issue Key") {
                label(issue.key)
            }
            field(text = "Summary") {
                label(issue.summary)
            }
            field(text = "Created on") {
                label(issue.dateCreated)
            }
            field(text = "Updated on") {
                label(issue.dateUpdated)
            }
        }
    }
}