package io.dante.virgil.ui.view

import io.dante.virgil.ui.controller.LoginController
import tornadofx.View
import tornadofx.ViewTransition
import tornadofx.action
import tornadofx.button
import tornadofx.label
import tornadofx.seconds
import tornadofx.textfield
import tornadofx.vbox

class LoginView : View() {
    val controller: LoginController by inject()

    override val root = vbox {
        styleClass.add("vbox")

        label("Virgil") {
            id = "app-header"
        }

        val usernameText = textfield {
            promptText = "Username"
        }

        val passwordText = textfield {
            promptText = "Password"
        }

        val baseUrlText = textfield {
            promptText = "Jira Base URL"
        }

        button ("Login") {
            id = "login-button"

            action {
                this.isDisable = true

                runAsync {
                    controller.login(
                        username = usernameText.text, password = passwordText.text, baseUrl = baseUrlText.text)
                } ui {
                    replaceWith(IssuesView::class, ViewTransition.Slide(0.3.seconds, ViewTransition.Direction.LEFT))
                }
            }
        }
    }
}