package io.dante.virgil.ui.controller

import io.dante.virgil.jira.Issue
import tornadofx.Controller
import tornadofx.SortedFilteredList

class IssuesController : Controller() {
    val issues = SortedFilteredList<Issue>()

    fun startWatching(): Unit {
        println("Started Watching")
    }

    fun stopWatching(): Unit {
        println("Stopped Watching")
    }
}